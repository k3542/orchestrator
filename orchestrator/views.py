import requests
from urllib import response
from django.shortcuts import render
from django.http import FileResponse, HttpResponse
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.decorators import action
import json
from django.conf import settings
from .models import Visit, ClinicVisit, PharmacyVisit
from .serializers import *

# Create your views here.


## FE view
class FrontendView(APIView):
    def get(self, request, id, format=None):
        print(id)
        all_visit = Visit.objects.filter(user_id=id)
        serializer = ModelVisitSerializer(instance=all_visit, many=True)
        data = serializer.data

        return Response(data)

## Clinic view
class ClinicPortView(APIView):    
    def post(self, request, format=None):
        serializer = ClinicResultSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        data = serializer.data

        print(data)

        target_clinic_visit = ClinicVisit.objects.get(pk=data["clinic_visit_id"])
        target_clinic_visit.doctor_recipes = data["doctor_recipes"]
        target_clinic_visit.diagnosis = data["diagnosis"]
        target_clinic_visit.save()

        # Lanjut ke APOTEK

        response = Response("OK", headers = {"Access-Control-Allow-Origin":"*"},status=200)    
        return response

## Pharmacy view

## Testing view
class TestingView(APIView):
    def get(self, request, id, format=None):
        print(id)
        all_visit = Visit.objects.filter(user_id=id)
        serializer = ModelVisitSerializer(instance=all_visit, many=True)
        data = serializer.data

        return Response(data)

    def post(self, request, id):
        serializer = CreateVisitSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        data = serializer.data

        print(data)

        new_visit = Visit.objects.create(
            user_id=serializer.data["user_id"],
            personal_recipes=serializer.data["personal_recipes"],
            symptoms=serializer.data["symptoms"]
        )

        new_visit.save()

        new_consult = ClinicVisit.objects.create(visit_id=new_visit)
        new_pharma = PharmacyVisit.objects.create(visit_id=new_visit)

        new_consult.save()
        new_pharma.save()

        pharma_id = new_pharma.id
        consult_id = new_consult.id

        ## Kirim ke Pharma
        ## Kirim ke Clinic
        url = "http://127.0.0.1:8001/clinicport/"
        headers = {'Content-Type': "application/json", 'Accept': "application/json"}
        send_clinic_data = {"clinic_visit_id":consult_id, "symptoms":new_visit.symptoms}
        serializer = ClinicSendSerializer(data=send_clinic_data)
        serializer.is_valid(raise_exception=True)
        data = serializer.data

        res = requests.post(url, json=data, headers=headers)
        print(res)

        return Response("OK")

    def delete(self, request, id):
        obj = Visit.objects.all()
        obj.delete()
        response = Response("OK", headers = {"Access-Control-Allow-Origin":"*"},status=200)     
        return response