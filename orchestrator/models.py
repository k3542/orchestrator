from django.db import models
from django.contrib.postgres.fields import ArrayField

# Create your models here.


class Visit(models.Model):
    user_id = models.IntegerField()
    personal_recipes = ArrayField(
        models.CharField(max_length=255), null=True, blank=True
    )
    symptoms = ArrayField(
        models.CharField(max_length=255), null=True, blank=True
    )

class ClinicVisit(models.Model):
    visit_id = models.ForeignKey(Visit, on_delete=models.CASCADE, related_name="visit_clinic")
    diagnosis = models.CharField(max_length=255, null=True, blank=True)
    doctor_recipes = ArrayField(
        models.CharField(max_length=255), null=True, blank=True
    )

class PharmacyVisit(models.Model):
    visit_id = models.ForeignKey(Visit, on_delete=models.CASCADE, related_name="visit_pharma")
    medicines = ArrayField(
        models.CharField(max_length=255), null=True, blank=True
    )