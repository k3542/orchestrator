# Generated by Django 3.2.9 on 2022-05-21 09:57

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('orchestrator', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='clinicvisit',
            name='visit_id',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='visit_clinic', to='orchestrator.visit'),
        ),
        migrations.AlterField(
            model_name='pharmacyvisit',
            name='visit_id',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='visit_pharma', to='orchestrator.visit'),
        ),
    ]
