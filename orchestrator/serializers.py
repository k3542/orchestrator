from rest_framework import serializers
from .models import *


class CreateVisitSerializer(serializers.Serializer):
    user_id = serializers.IntegerField()
    personal_recipes = serializers.ListField(child=serializers.CharField())
    symptoms = serializers.ListField(child=serializers.CharField(), default=[])

class ClinicSendSerializer(serializers.Serializer):
    clinic_visit_id = serializers.IntegerField()
    symptoms = serializers.ListField(child=serializers.CharField(), default=[])

class ClinicResultSerializer(serializers.Serializer):
    clinic_visit_id = serializers.IntegerField()
    doctor_recipes = serializers.ListField(child=serializers.CharField())
    diagnosis = serializers.CharField(max_length=255)


class ClinicVisitSerializer(serializers.ModelSerializer):
    class Meta:
        model = ClinicVisit
        fields = ['id', 'visit_id', 'diagnosis', 'doctor_recipes']

class PharmacyVisitSerializer(serializers.ModelSerializer):
    class Meta:
        model = PharmacyVisit
        fields = ['id', 'visit_id', 'medicines']


class ModelVisitSerializer(serializers.ModelSerializer):
    visit_clinic = ClinicVisitSerializer(many=True, read_only=True)
    visit_pharma = PharmacyVisitSerializer(many=True, read_only=True)
    class Meta:
        model = Visit
        fields = ['id', 'user_id', 'personal_recipes', 'symptoms','visit_clinic','visit_pharma']
