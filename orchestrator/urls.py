from django.urls import path
from .views import *

urlpatterns = [
    path('clinicport/', ClinicPortView.as_view(), name='url_clinic'),
    path('frontendport/<int:id>/', FrontendView.as_view(), name='url_fe'),
    path('testfrontend/<int:id>/', TestingView.as_view(), name='url_test_fe')
]
